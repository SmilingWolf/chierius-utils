#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main(int argc, const char* argv[])
{
    HANDLE inFile;
    HANDLE outFile;
    uint32_t dwBytesWritten;
    int8_t Filename[FILENAME_MAX] = { 0x00 };
    uint32_t i;

    uint8_t *FileBuffer;
    uint32_t FileSize;

    if (argc < 2)
    {
        printf("%s usage:\n", argv[0]);
        printf("How 'bout you supply a filename?\n");
        exit(EXIT_FAILURE);
    }

    inFile = CreateFile(argv[1], GENERIC_READ, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    FileSize = GetFileSize(inFile, NULL);
    FileBuffer = VirtualAlloc(NULL, FileSize, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
    ReadFile(inFile, FileBuffer, FileSize, &dwBytesWritten, NULL);

    /*
    In the ISS script header:
        uint32_t    ScriptStartOffset
        uint32_t    ScriptSignature     // Some kind of "script type" signature, can vary. The standard appears to be 00009795
    These 8 bytes MUST be left in cleartext
    */
    i = 8;
    while (i < FileSize)
    {
        FileBuffer[i] = (FileBuffer[i] >> 6) | (FileBuffer[i] << 2);
        i++;
    }

    sprintf(Filename, "%s.enc", argv[1]);
    outFile = CreateFileA(Filename, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
    WriteFile(outFile, FileBuffer, FileSize, &dwBytesWritten, 0);

    VirtualFree(FileBuffer, 0, MEM_RELEASE);
    CloseHandle(inFile);
}
