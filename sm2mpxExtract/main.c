#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, const char* argv[])
{
    HANDLE inFile;
    HANDLE outFile;
    unsigned char *IndexBuffer;
    unsigned long dwBytesWritten;
    unsigned long i;

    unsigned char Header[32] = { 0x00 };
    unsigned long NumOfEntries;
    unsigned long HeaderIndexLength;
    unsigned char ArchiveName[12] = { 0x00 };

    unsigned char ArchivedFileName[13] = { 0x00 };
    unsigned long ArchivedFileOffset;
    unsigned long ArchivedFileLength;
    unsigned char *FileBuffer;

    if (argc < 2)
    {
        printf("%s usage:\n", argv[0]);
        printf("How 'bout you supply a filename?\n");
        exit(EXIT_FAILURE);
    }

    inFile = CreateFile(argv[1], GENERIC_READ, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

    ReadFile(inFile, Header, sizeof(Header), &dwBytesWritten, NULL);

    if (memcmp(Header, "SM2MPX10", 8) != 0)
    {
        printf("Please check yo shit\n");
        CloseHandle(inFile);
        exit(EXIT_FAILURE);
    }

    NumOfEntries = Header[8] | (Header[9] << 8) | (Header[10] << 16) | (Header[11] << 24);
    HeaderIndexLength = Header[12] | (Header[13] << 8) | (Header[14] << 16) | (Header[15] << 24);
    memcpy(ArchiveName, &Header[16], sizeof(ArchiveName));

    printf("Number of files in the archive:\t%lu\t(hex: %.08X)\n", NumOfEntries, NumOfEntries);
    printf("Length of the archive header:\t%lu\t(hex: %.08X)\n", HeaderIndexLength, HeaderIndexLength);
    printf("Archive Name: %s\n", ArchiveName);

    IndexBuffer = VirtualAlloc(NULL, HeaderIndexLength, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
    ReadFile(inFile, IndexBuffer, HeaderIndexLength, &dwBytesWritten, NULL);

    for (i = 0; i < NumOfEntries; i++)
    {
        memset(ArchivedFileName, 0, sizeof(ArchivedFileName));
        memcpy(ArchivedFileName, &IndexBuffer[i * 20], sizeof(ArchivedFileName) - 1);
        ArchivedFileOffset = IndexBuffer[i * 20 + 12] | (IndexBuffer[i * 20 + 13] << 8) | (IndexBuffer[i * 20 + 14] << 16) | (IndexBuffer[i * 20 + 15] << 24);
        ArchivedFileLength = IndexBuffer[i * 20 + 16] | (IndexBuffer[i * 20 + 17] << 8) | (IndexBuffer[i * 20 + 18] << 16) | (IndexBuffer[i * 20 + 19] << 24);

#ifdef DEBUG
        printf("File name:\t%s\n", ArchivedFileName);
        printf("File offset:\t%lu\t(hex: %.08X)\n", ArchivedFileOffset, ArchivedFileOffset);
        printf("File length:\t%lu\t(hex: %.08X)\n\n", ArchivedFileLength, ArchivedFileLength);
#endif // DEBUG

        outFile = CreateFile(ArchivedFileName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
        FileBuffer = VirtualAlloc(NULL, ArchivedFileLength, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
        SetFilePointer(inFile, ArchivedFileOffset, NULL, SEEK_SET);
        ReadFile(inFile, FileBuffer, ArchivedFileLength, &dwBytesWritten, NULL);
        WriteFile(outFile, FileBuffer, ArchivedFileLength, &dwBytesWritten, NULL);
        VirtualFree(FileBuffer, 0, MEM_RELEASE);
        CloseHandle(outFile);
    }

    VirtualFree(IndexBuffer, 0, MEM_RELEASE);
    CloseHandle(inFile);

    return 0;
}
