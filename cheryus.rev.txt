00439F90: Start of the script reading function (probably...)
0043A0F0: The decryption routine itself

ISF file format:
uint32_t    ScriptStartOffset
uint32_t    ScriptSignature     // Some kind of "script type" signature. Can vary. The standard appears to be 00009795

The two above followed by a table of:
uint32_t    SEntryRelOffset     // Relative (to ScriptStartOffset) offset of a script entry. Calc as ScriptStartOffset + SEntryRelOffset
