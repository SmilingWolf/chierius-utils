#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main(int argc, const char* argv[])
{
    HANDLE inFile;
    HANDLE outFile;
    int8_t Filename[FILENAME_MAX] = { 0x00 };
    uint32_t dwBytesWritten;

    uint8_t GgpHeader[36];
    uint32_t PngLength;
    uint32_t PngOffset;
    uint32_t i;
    uint8_t XorKey;
    uint8_t *PngBuffer;

    if ( argc < 2 )
    {
        printf("%s usage:\n", argv[0]);
        printf("How 'bout you supply a filename?\n");
        exit(1);
    }

    inFile = CreateFileA(argv[1], GENERIC_READ, 0, 0, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
    ReadFile(inFile, GgpHeader, sizeof(GgpHeader), &dwBytesWritten, 0);

    if ( memcmp(GgpHeader, "GGPFAIKE", 8) )
    {
        printf("Please check yo shit\n");
        CloseHandle(inFile);
        exit(1);
    }

    PngLength = GgpHeader[24] | ((GgpHeader[25] | ((GgpHeader[26] | (GgpHeader[27] << 8)) << 8)) << 8);
    PngOffset = GgpHeader[32] | ((GgpHeader[33] | ((GgpHeader[34] | (GgpHeader[35] << 8)) << 8)) << 8);

    PngBuffer = VirtualAlloc(0, PngLength, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
    SetFilePointer(inFile, PngOffset + sizeof(GgpHeader), 0, 0);
    ReadFile(inFile, PngBuffer, PngLength, &dwBytesWritten, 0);

    i = 0;
    if ( PngLength )
    {
        do
        {
            XorKey = GgpHeader[i & 7] ^ GgpHeader[(i & 7) + 12];
            *(PngBuffer + i) ^= XorKey;
            ++i;
        }
        while ( i < PngLength );
    }

    sprintf(Filename, "%s.png", argv[1]);
    outFile = CreateFileA(Filename, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
    WriteFile(outFile, PngBuffer, PngLength, &dwBytesWritten, 0);

    VirtualFree(PngBuffer, 0, MEM_RELEASE);
    CloseHandle(outFile);
    CloseHandle(inFile);
    return 0;
}
