#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main(int argc, const char* argv[])
{
    HANDLE inFile;
    uint32_t dwBytesWritten;
    int8_t Filename[FILENAME_MAX] = { 0x00 };
    uint32_t i;
    uint32_t j;
    uint32_t z;
    uint32_t y;

    uint8_t *FileBuffer;
    uint32_t FileSize;

    uint32_t ScriptStartOffset;
    uint32_t ScriptSignature;     // Some kind of "script type" signature. Can vary. The standard appears to be 00009795
    uint32_t NumOfEntries;
    uint32_t SEntryRelOffset;     // Relative (to ScriptStartOffset) offset of a script entry. Calc as ScriptStartOffset + SEntryRelOffset
    uint32_t SEntryAbsOffset;
    uint32_t SNextEntryRelOffset;
    uint32_t SNextEntryAbsOffset;
    uint32_t SEntryLength;
    uint8_t Opcode;
    uint16_t OpcodeLength;
    uint8_t *OpcodeData;
    uint32_t CharacterID;
    uint8_t SpeechFileName[8] = { 0x00 };
    uint8_t PrintedString[50] = { 0x00 };

    if (argc < 2)
    {
        printf("%s usage:\n", argv[0]);
        printf("How 'bout you supply a filename?\n");
        exit(EXIT_FAILURE);
    }

    inFile = CreateFile(argv[1], GENERIC_READ, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    FileSize = GetFileSize(inFile, NULL);
    FileBuffer = VirtualAlloc(NULL, FileSize, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
    ReadFile(inFile, FileBuffer, FileSize, &dwBytesWritten, NULL);

    ScriptStartOffset = FileBuffer[0] | (FileBuffer[1] << 8) | (FileBuffer[2] << 16) | (FileBuffer[3] << 24);
    ScriptSignature = FileBuffer[4] | (FileBuffer[5] << 8) | (FileBuffer[6] << 16) | (FileBuffer[7] << 24);

    printf("Script start: %.08X\n", ScriptStartOffset);
    printf("Script sign:  %.08X\n", ScriptSignature);

    NumOfEntries = (ScriptStartOffset - (sizeof(ScriptStartOffset) + sizeof(ScriptSignature)));
    NumOfEntries /= 4;

    printf("# of entries: %i\n\n", NumOfEntries);

    for (i = 0; i < NumOfEntries; i++)
    {
        SEntryRelOffset = FileBuffer[i * 4 + 8] | (FileBuffer[i * 4 + 8 + 1] << 8) | (FileBuffer[i * 4 + 8 + 2] << 16) | (FileBuffer[i * 4 + 8 + 3] << 24);
        SEntryAbsOffset = SEntryRelOffset + ScriptStartOffset;

#ifdef DEBUG
        printf("Entry #%i at: %.08X\n", i, SEntryAbsOffset);
#endif // DEBUG

        if (i < NumOfEntries - 1)
        {
            i++;
            SNextEntryRelOffset = FileBuffer[i * 4 + 8] | (FileBuffer[i * 4 + 8 + 1] << 8) | (FileBuffer[i * 4 + 8 + 2] << 16) | (FileBuffer[i * 4 + 8 + 3] << 24);
            SNextEntryAbsOffset = SNextEntryRelOffset + ScriptStartOffset;
            SEntryLength = SNextEntryAbsOffset - SEntryAbsOffset;
            i--;
        }
        else
        {
            SEntryLength = FileSize - SEntryAbsOffset;
        }

#ifdef DEBUG
        printf("Entry length: %.08X\n", SEntryLength);
#endif // DEBUG

        for (j = 0; j < SEntryLength; j++)
        {
            Opcode = FileBuffer[SEntryAbsOffset + j];

#ifdef DEBUG
            printf("Opcode: %.02X\n", Opcode);
#endif // DEBUG

            if (FileBuffer[SEntryAbsOffset + j + 1] > 0x7F)
            {
                OpcodeLength = ((FileBuffer[SEntryAbsOffset + j + 1] & 0x7F) << 8) | FileBuffer[SEntryAbsOffset + j + 2];
            }
            else
            {
                OpcodeLength = FileBuffer[SEntryAbsOffset + j + 1];
            }

            // The script contains opcodes for the interpreter
            // Opcode 0x2B means that what follows will regard the dialogue
            // (Text, spoken lines... that kind of things)
            if (Opcode == 0x2B)
            {
                printf("Print OP Offset: %.08X\n", SEntryAbsOffset + j);
                printf("Print OP Len: %.08X\n", OpcodeLength);

                OpcodeData = VirtualAlloc(NULL, OpcodeLength, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
                memcpy(OpcodeData, &FileBuffer[SEntryAbsOffset + j], OpcodeLength);

                for (y = 0; y < OpcodeLength; y++)
                {
                    // Opcode 0x11 sets the ID of the talking character
                    // The ID <-> Name association takes place in START.ISF
                    if (OpcodeData[y] == 0x11)
                    {
                        y++;
                        CharacterID = (OpcodeData[y]) | (OpcodeData[y + 1] << 8) | (OpcodeData[y + 2] << 16) | (OpcodeData[y + 3] << 24);
                        printf("Character ID: %.08X\n", CharacterID);
                        y += 3;
                    }

                    // Opcode 0x13 means that what follows will be the name of the voice file
                    // with the spoken line of the current character and dialogue text
                    else if (OpcodeData[y] == 0x13)
                    {
                        y++;
                        memset(SpeechFileName, 0, sizeof(SpeechFileName));
                        memcpy(SpeechFileName, &OpcodeData[y], sizeof(SpeechFileName));
                        printf("Speech File: %s\n", SpeechFileName);
                        y += 7;
                    }

                    // Opcode 0xFF means that what follows is a string to be printed
                    else if (OpcodeData[y] == 0xFF)
                    {
                        y++;
                        z = 0;
                        printf("String: ");

                        // Opcode 0x7F means that the following byte is part of the string
                        while (OpcodeData[y] == 0x7F)
                        {
                            if (z < sizeof(PrintedString) - 1)
                            {
                                PrintedString[z] = OpcodeData[y + 1];
                                y += 2;
                                z++;
                            }
                            else
                            {
                                printf("%s", PrintedString);
                                memset(PrintedString, 0, sizeof(PrintedString));
                                z = 0;
                            }
                        }
                        if (z != 0)
                        {
                            printf("%s\n", PrintedString);
                            memset(PrintedString, 0, sizeof(PrintedString));
                            z = 0;
                        }
                    }
                }
                memset(OpcodeData, 0, sizeof(OpcodeData));
                VirtualFree(OpcodeData, 0, MEM_RELEASE);
            }
            j += OpcodeLength - 1;
        }
        printf("\n");
    }
    VirtualFree(FileBuffer, 0, MEM_RELEASE);
    CloseHandle(inFile);
}
